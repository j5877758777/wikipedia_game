# Wikipedia Game
---

## Environment

* Install by anaconda
```
conda create -n myenv python=3.6.7
conda install -c conda-forge wikipedia
```
* By pip
```
pip install wikipedia
```
## Execution

```
python wikipedia_game.py [start_page] [end_page] [BFS_type] [worker_num]
```

* start_page : wikipedia starting page, **default : "Web Bot"**

* end_page : Target page, **default : "Tax Holiday"**

* BFS_type : Different BFS type, **default : single_tuple**
    * single_tuple : BFS using namedtuple to build tree
    * single_class : BFS using class with `__slot__` to build tree
    * concur : BFS using namedtuple with concurrent(multi-thread)
    * multi : BFS using namedtuple with multi-processing(multi-CPU) 

* worker_num: define worker numbers when using concur(max:100) or multi(max:5), **default : 20**
    * concur: Create other
[worker_num] threads to get the wiki page.
    * multi: Use other [worker_num] CPU core to get the wiki page and deal with Exception. 
### Example
```
python wikipedia_game.py
python wikipedia_game.py "Web Bot" "Tax Holiday"
python wikipedia_game.py "Web Bot" "Tax Holiday" "single_class"
python wikipedia_game.py "Web Bot" "Tax Holiday" "concur" 20
```

## Performance Test
test case : Barack Obama -> Tax credit -> Tax holiday

### Memory usage (Base on Experiment.ipynb)

| class without `__slot__` | class without `__slot__` | namedtuple |
| --------|---------|-------|
|256.1MB|156.2MB|138.6MB|

### Time consume (Base on Experiment.ipynb and multiprocessing.png)

|| class without `__slot__`  | namedtuple | concur 5 | concur 20 | concur 50 | concur 5 | multiprocessing 6|
|---| ----|-----|-----|----|----|----|----|
|CPU TIME(s)|252.35938|293.45312|279.50000|254.17188|251.10938|246.67188|89.18750|
|WALL TIME(s)|1438.86742|998.86456|686.06242|632.06770|687.34637|666.20368|522.74035|

* p.s. Different result in concurrent: Because multithreading will require many page at a short time but can't keep the order when getting responses.

* p.s. RATE_LIMIT_MIN_WAIT
> In the wikipedia package  
wikipedia.py -> set_rate_limiting function -> RATE_LIMIT_MIN_WAIT  default with milliseconds=50 between every call.

* p.s. Multiprocessing are more unstable in Jupyter-notebook, so I use command line to run code.
![Multiprocessing](/multiprocessing.png)

## Further Improvement

1. Change the RATE_LIMIT_MIN_WAIT, but need more testing.
2. When using the wikipedia api to get the links, it will automatically sorted the links by alphabet.   
But by the tip of Wikipedia game, you will have better change to get the target page when searching on the first few links.     
So better not use the wikipedia api to get the sublinks.