import wikipedia
import warnings
warnings.filterwarnings("ignore") # To get rid of BeautifulSoup warning on windows 10
'''
UserWarning: No parser was explicitly specified, 
so I'm using the best available HTML parser for this system ("html.parser").
This usually isn't a problem, but if you run this code on another system, 
or in a different virtual environment, it may use a different parser and behave differently.
'''

# from functool import partial
# get_page = partial(wikipedia.page, auto_suggest=False);
def get_page(title):
    '''
    Get the wikipedia page information by wikipedia API
    '''
    return wikipedia.page(title=title, auto_suggest=False)

def get_path(leaf_node):
    '''
    Input the leaf node(whether the type of namedtuple or class)
    Return the path from root to leaf node.
    '''
    print('\n')

    # Insert the node path from leaf to root.
    path = []
    while leaf_node is not None:
        path.append(leaf_node.title)
        leaf_node = leaf_node.parent
    
    # Reverse the list order
    path.reverse()
    print(' -> '.join(path))