import logging
import os 

class LogController(object):
    '''
    Logging controller.     
    Setting logger, default format is '%(asctime)s %(name)-8s %(levelname)-8s %(message)s'  
    Args :
        log_name : setup the name of logger.
        logger_dir : setup the dir of log file.
    '''    
    def __init__(self, logger_name, logger_dir):

        if os.path.exists(logger_dir) is False:
            os.mkdir(logger_dir)
        log_path = logger_dir + "/" + logger_name + ".log"

        self.logging_setup(logger_name, log_path)

    def __call__(self, log_level, message):
        '''
        Args:   
            log_level: 'DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'
            message: message string

        e.x. LogController('INFO', 'Log example')
        '''
        self.log_func[log_level](message)

    def logging_setup(self, log_name, log_path):
        '''
        Setting logger, default format is '%(asctime)s %(name)-8s %(levelname)-8s %(message)s'  
        Args :
            log_name : setup the name of logger.
            log_path : setup the path of log file.
        '''
        formatter = logging.Formatter('%(asctime)s %(name)-8s %(levelname)-8s %(message)s',  datefmt='%Y-%m-%d %H:%M:%S')

        handler = logging.FileHandler(log_path, 'a', 'utf-8')        
        handler.setFormatter(formatter)

        logging.getLogger("urllib3").setLevel(logging.WARNING)
        logging.getLogger("requests").setLevel(logging.ERROR) # logging.WARNING
        logging.getLogger("BeautifulSoup").setLevel(logging.ERROR)

        self.logger = logging.getLogger(log_name)
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(handler)
        self.log_func = {
            'INFO': self.logger.info,
            'DEBUG': self.logger.debug,
            'WARNING': self.logger.warning,
            'ERROR': self.logger.error,
            'CRITICAL': self.logger.critical,
        }

    def logging_setup_another(self, log_name, log_path='Log.log'):
        '''
        Setting logger, default format is '%(asctime)s %(name)-8s %(levelname)-8s %(message)s'  
        Args :
            log_name : setup the name of logger.
            log_path : setup the path of log file.
        '''
        logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(name)-8s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M:%S',
                        handlers = [logging.FileHandler(log_path, 'a', 'utf-8'),])

        console = logging.StreamHandler()
        console.setLevel(logging.INFO)

        formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
        console.setFormatter(formatter)

        logging.getLogger("urllib3").setLevel(logging.WARNING)
        logging.getLogger("requests").setLevel(logging.WARNING)
        
        logging.getLogger(log_name).addHandler(console)

        self.logger = logging.getLogger(log_name)
        self.log_func = {
            'INFO': self.logger.info,
            'DEBUG': self.logger.debug,
            'WARNING': self.logger.warning,
            'ERROR': self.logger.error,
            'CRITICAL': self.logger.critical,
        }

    