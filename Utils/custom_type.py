from collections import namedtuple

Node_tuple = namedtuple('Node_tuple', ['title', 'parent'])

# use in Method/multicore_BFS
Result_tuple = namedtuple('Result_tuple', ['result', 'parent'])


class Node_class():
    __slots__ = ["title","parent"]
    def __init__(self, title, parent = None):
        self.title = title
        self.parent = parent