import time
import functools


def clock(func):
    '''
    Print the function CPU time, WALL time

    Use functools.wraps to
    wraps all variable include function name into the decorate
    To preform better view of original function
    '''
    @functools.wraps(func)
    def clocked(*args, **kwargs):
        t0 = time.perf_counter()
        CPU_t0 = time.process_time() 
        result = func(*args, **kwargs)
        CPU_elapsed = time.process_time() - CPU_t0
        elapsed = time.perf_counter() - t0
        name = func.__name__
        arg_lst = []

        if args:
            arg_lst.append(', '.join(repr(arg) for arg in args))
        if kwargs:
            pairs = ['%s=%r' % (k, w) for k, w in sorted(kwargs.items())]
            arg_lst.append(', '.join(pairs))  

        arg_str = ', '.join(arg_lst)
        print('CPU time: %0.5fs' % CPU_elapsed)
        print('WALL time: %0.5fs' % elapsed)
        return result

    return clocked


