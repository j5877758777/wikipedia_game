from Utils.custom_type import Node_class, Node_tuple, Result_tuple
from Utils.wiki_helper import get_page
import sys, time
from wikipedia.exceptions import PageError
from wikipedia import WikipediaPage
from queue import Empty

# result -> page or Error , parent -> parent Node
from multiprocessing import Process, Value, Queue

def get_page_process(stop, node_queue, result_queue):
    while stop.value == 0:
        if node_queue.empty():
            time.sleep(0.1)
        else:
            break
    while stop.value == 0:
        # print(stop.value)
        try:
            parent_tuple = node_queue.get(timeout=1)
            curr_title = parent_tuple[0]
            curr_page = get_page(title=curr_title)
            result = Result_tuple(curr_page, parent_tuple)
            result_queue.put(result,timeout=1)
        except Empty:
            continue
        except Exception as e:
            result = Result_tuple(e, parent_tuple)
            result_queue.put(result,timeout=1)
    
    while not node_queue.empty():
        try:
            _ = node_queue.get(timeout=1)
        except:
            pass
    while not result_queue.empty():
        try:
            _ = result_queue.get(timeout=1)
        except:
            pass
    
    print('worker stopped.')
            
def mul_BFS(start_page:str, end_page:str, Logger, worker_num, **dump):
    '''
    Create {worker_num} process to get and check with wiki page. 
    Construct Tree with namedtuple.

    Diff with CONCUR_BFS: because of GIL(GlobalInterpreterLock) in CPython,     
    concur_BFS can only create Thread in the same CPU.
    '''
    stop = Value('i', 0) # 0 False
    node_queue = Queue()
    result_queue = Queue()

    repeat_set = set()
    root = Node_tuple(start_page, None)
    node_queue.put(root)
    # root_2 = Node_tuple("Tax credit", None)
    # node_queue.put(root_2)
    
    workers = []

    for _ in range(worker_num):
        worker = Process(target=get_page_process, args=(stop, node_queue,result_queue,))
        worker.start()
        workers.append(worker)
    
    time.sleep(1) # to make sure 
    try:
        while True:
            if node_queue.empty() and result_queue.empty():
                for worker in workers:
                    worker.terminate()
                return
            if result_queue.empty():
                time.sleep(0.1)
                continue
            result = result_queue.get()
            if not isinstance(result[0], WikipediaPage):
                Logger(log_level="ERROR", message="{} :{}".format(result[0].__class__.__name__, result[0]))
                continue
            print("{} Searching: {}\t".format(len(repeat_set), result[1].title), end='\r')
            sys.stdout.flush()

            try:
                if end_page in set(result[0].links):
                    print('\n')
                    print('*'*60)
                    print('Number of searches: {}'.format(len(repeat_set)+1))
                    print('start clear all queue')
                    stop.value = 1 # stop others process
                    for worker in workers:
                        worker.join(10)
                    
                    return Node_tuple(end_page, result[1])
            except PageError as e:
                # Some page won't provide the next link(protected link) e.x. get_page("Protected page", Logger)
                Logger(log_level="WARNING", message="{} :{}".format(e.__class__.__name__, e))
                continue

            repeat_set.add(result[1].title)
            for title in result[0].links:
                if title in repeat_set:
                    continue
                child = Node_tuple(title, result[1])
                node_queue.put(child)
    except Exception as e:
        Logger(log_level="INFO", message="{} :{}".format(e.__class__.__name__, e))
        stop.value = 1
        # node_queue.close()
        # node_queue.join_thread()
        # result_queue.close()
        # result_queue.join_thread()
        for worker in workers:
            worker.join(10)
        sys.exit(0)