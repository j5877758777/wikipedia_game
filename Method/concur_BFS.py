from Utils.custom_type import Node_class, Node_tuple
from Utils.wiki_helper import get_page
import sys

# concurrent
from concurrent import futures
import queue

def CONCUR_BFS(start_page:str, end_page:str, Logger, worker_num, **dump) -> Node_tuple:
    '''
    Create {worker_num} thread to help dealing with heavy network I/O.
    Construct Tree with namedtuple.
    '''
    q = queue.Queue()
    repeat_set = set()
    root = Node_tuple(start_page, None)
    q.put(root)
    
    with futures.ThreadPoolExecutor(max_workers=worker_num) as executor:
        while True:
            if q.empty():
                return None
            to_do_map = {}
            for i in range(worker_num):
                if q.empty():
                    break

                parent_tuple = q.get()
                curr_title = parent_tuple[0]

                # If already been search continue to the next title.
                if curr_title in repeat_set:
                    continue

                # Create concurrent thread
                future = executor.submit(get_page, curr_title)
                to_do_map[future] = parent_tuple

            done_page = futures.as_completed(to_do_map)
            for future in done_page:
                try:
                    curr_page = future.result()
                except Exception as e:
                    Logger(log_level="ERROR", message="{} :{}".format(e.__class__.__name__, e))
                else:
                    # Check if the page in the sublinks
                    try:
                        if end_page in set(curr_page.links):
                            print('\n')
                            print('*'*60)
                            print('Number of searches: {}'.format(len(repeat_set)+1))
                            return Node_tuple(end_page, to_do_map[future])
                    except Exception as e:
                        # Some page won't provide the next link(protected link) e.x. get_page("Protected page", Logger)
                        Logger(log_level="ERROR", message="{} :{}".format(e.__class__.__name__, e))
                        continue
                    else:
                        print("{} Searching: {}\t".format(len(repeat_set), to_do_map[future].title), end='\r')
                        sys.stdout.flush()
                        # Add the current title to the repeat_set and add all the sublink to the queue.
                        repeat_set.add(to_do_map[future].title)
                        for title in curr_page.links: # spend time...
                            child = Node_tuple(title, to_do_map[future])
                            q.put(child)
