from Utils.custom_type import Node_class, Node_tuple
from Utils.wiki_helper import get_page
import sys


def BFS_search_tuple(start_page:str, end_page:str, Logger, **dump) -> Node_tuple:
    '''
    Using BFS to search from the start_page to end_page.
    Construct Tree with namedtuple to save more memory.
    '''
    repeat_set = set()
    queue = [] # BFS order queue
    root = Node_tuple(start_page, None)
    queue.append(root)
    
    while True:
        parent_tuple = queue.pop(0)
        curr_title = parent_tuple[0]

        # If already been search continue to the next title.
        if curr_title in repeat_set:
            continue
        
        # Print the Node number and the searching title
        print("{} Searching: {}\t".format(len(repeat_set), curr_title), end='\r')
        sys.stdout.flush()

        try:
            # Get page
            curr_page = get_page(curr_title)
        except Exception as e:
            # Error when get_page happen
            Logger(log_level="ERROR", message="{} :{}".format(e.__class__.__name__, e))
            Logger(log_level="INFO", message="The parent of {} is {}".format(curr_title, parent_tuple[1][0]))
            continue
        
        # Check if the page in the sublinks
        try:
            if end_page in set(curr_page.links):
                print('\n')
                print('*'*60)
                print('Number of searches: {}'.format(len(repeat_set)+1))
                
                return Node_tuple(end_page, parent_tuple)
                
        except Exception as e:
            # Some page won't provide the next link(protected link) e.x. get_page("Protected page", Logger)
            Logger(log_level="ERROR", message="{} :{}".format(e.__class__.__name__, e))
            continue
        
        # Add the current title to the repeat_set and add all the sublink to the queue.
        repeat_set.add(curr_title)
        queue += [ Node_tuple(title, parent_tuple) for title in curr_page.links]

def BFS_search_class(start_page:str, end_page:str, Logger, **dump) -> Node_class:
    '''
    Using BFS to search from the start_page to end_page.
    Construct Tree with class and __slot__ to save more memory.
    '''
    repeat_set = set()
    queue = [] # BFS order queue
    root = Node_class(start_page)
    queue.append(root)
    
    while True:
        parent_node = queue.pop(0)
        curr_title = parent_node.title
        
        # If already been search continue to the next title.
        if curr_title in repeat_set:
            continue
        
        # Print the Node number and the searching title
        print("{} Searching: {}\t".format(len(repeat_set), curr_title), end='\r', flush=True)
        sys.stdout.flush()

        try:
            # Get page
            curr_page = get_page(curr_title)
        except Exception as e:
            # Error when get_page happen
            Logger(log_level="ERROR", message="{} :{}".format(e.__class__.__name__, e))
            Logger(log_level="INFO", message="The parent of {} is {}".format(curr_title, parent_node.parent.title))
            continue
        
        # Check if the page in the sublinks
        try:
            if end_page in set(curr_page.links):
                print('\n')
                print('*'*60)
                print('Number of searches: {}'.format(len(repeat_set)+1))

                return Node_class(title=end_page, parent=parent_node)
                
        except Exception as e:
            # Some page won't provide the next link(protected link) e.x. get_page("Protected page", Logger)
            Logger(log_level="ERROR", message="{} :{}".format(e.__class__.__name__, e))
            continue
        
        repeat_set.add(curr_title)
        queue += [Node_class(title=title, parent=parent_node) for title in curr_page.links]