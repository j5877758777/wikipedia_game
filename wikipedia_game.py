from Utils.LogController import LogController
from Utils.wiki_helper import get_path
from Utils.Clock import clock
import Method

import sys

# A little protect
MAX_CONCUR_WORKER = 100
MAX_CPU_WORKER = 5


@clock
def main(BFS_search, *args, **kwargs):
    '''
    Start search and print the result.
    '''
    leaf_node = BFS_search(*args, **kwargs)
    get_path(leaf_node)

def parse_argv():
    '''
    Read input argument to variables.
    I prefer using argparse package
    '''
    default_argv = {
        "start_page": "Web Bot",
        "end_page": "Tax holiday",
        "BFS_type" : "single_tuple",
        "worker_num" : 20
    }

    try:
        default_argv['start_page'] = sys.argv[1]
        default_argv['end_page'] = sys.argv[2]
        default_argv['BFS_type'] = sys.argv[3]
        default_argv['worker_num'] = int(sys.argv[4])
    except IndexError:
        # list out of range
        pass
    except ValueError:
        print('ValueError: worker_num input type should be INTEGER')
        sys.exit(0)

    if default_argv['BFS_type'] == 'single_tuple':
        BFS_search = Method.BFS_search_tuple
        default_argv['worker_num'] = 1

    elif default_argv['BFS_type'] == 'single_class':
        BFS_search = Method.BFS_search_class
        default_argv['worker_num'] = 1

    elif default_argv['BFS_type'] == 'concur':
        BFS_search = Method.CONCUR_BFS
        default_argv['worker_num'] = min(MAX_CONCUR_WORKER, default_argv.worker_num)

    elif default_argv['BFS_type'] == 'multi':
        BFS_search = Method.mul_BFS
        default_argv['worker_num'] = min(MAX_CPU_WORKER, default_argv.worker_num)

    else:
        BFS_search = Method.BFS_search_tuple
        default_argv['worker_num'] = 1

    return BFS_search, default_argv


if __name__ == '__main__':
    
    BFS_search, default_argv = parse_argv()
    print("BFS type :{BFS_type}\tworker_num:{worker_num}".format(**default_argv))

    Logger = LogController(logger_name="Wiki", logger_dir="Log")
    main(BFS_search=BFS_search, Logger=Logger, **default_argv)